# my standart filetree
this setup i use is primarily focused on sync for almost all of my files exept things i only need local like isos and software. 
`/` is the home directory

| Folder           | Contents                                    |
| -----------------| --------------------------------------------|
| /Desktop         | files i actively work on
| /Downloads       | here i keep my downloads and cloned projects
| /Downloads/aur   | here i keep aur packages
| /Downloads/iso   | here i keep my iso files
| /Cloud           | this is the root folder i sync
| /Cloud/Arbeit    | work related files
| /Cloud/Archiv    | zips and tarballs for longtime storage
| /Cloud/Bilder    | my pictures
| /Cloud/Dokumente | my documents
| /Cloud/Familie   | a folder that is shared with my family
| /Cloud/KeePass   | my important files
| /Cloud/Musik     | my music and audio
| /Cloud/Musik     | my music and audio
| /Cloud/Schule    | school related files
| /Cloud/Upload    | folder i use to Uplade files
| /Cloud/Videos    | my videos
